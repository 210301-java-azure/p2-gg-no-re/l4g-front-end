import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private http: HttpClient) { }

  uploadImage(formData: FormData): Observable<any> {
    console.log("Uplaod service callled");
    return this.http.post(environment.baseUrl + `image/upload`, formData);
  }



  getImageByGroupId(groupId: number): Observable<any> {
    var headers = new HttpHeaders({ responseType: "blob" })
    return this.http.get(environment.baseUrl + `image/group/${groupId}`, { headers: headers });
  }

  deleteImageById(groupId: number): Observable<any> {
    console.log("this was called");
    return this.http.delete(environment.baseUrl + `image/${groupId}`);
  }
}
