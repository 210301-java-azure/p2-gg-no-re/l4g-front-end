import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Group } from '../groups.component';
import { GroupsService } from '../groups.service';
import { Observable } from 'rxjs';
import { UploadService } from 'src/app/upload.service';
import { DomSanitizer } from '@angular/platform-browser';




@Component({
  selector: 'app-group-home',
  templateUrl: './group-home.component.html',
  styleUrls: ['./group-home.component.css']
})
export class GroupHomeComponent implements OnInit {
  inGroup = true;
  group: Group | null = null;
  members: any[] = [];
  editingGroup: boolean = false;
  fileName = "";
  userId: any = sessionStorage.getItem("userId");
  data: any;
  image: any;
  imageUrl: any;

  constructor(private router: Router, private gs: GroupsService, private uploadService: UploadService, private domSanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.group = this.gs.getGroup();
    if (this.group == null)
      this.router.navigate(['/groups'])

    if (this.group)
      this.gs.getUsersOfGroup((<Group>this.group).id)
        .subscribe(res => {
          console.log(res)
          this.members = res;
        }, rej => {
          console.log(rej)
        })

    var groupId = (<Group>this.group).id
    let asyncDisplayImage: Observable<any> = this.uploadService.getImageByGroupId(groupId);
    asyncDisplayImage.subscribe(fileReturned => {
      this.data = fileReturned;
      this.image = this.data.image;
      return this.imageUrl = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,' + this.image)
    });
  }

  leaveGroup() {
    var userId = parseInt(<string>sessionStorage.getItem("userId"))
    this.kickUser(userId, true);
  }

  setEditing(val: boolean) {
    this.editingGroup = val;
  }

  editGroup() {
    var id = (<Group>this.group).id;
    var name = (<HTMLInputElement>document.getElementById("edit-name")).value;
    var desc = (<HTMLTextAreaElement>document.getElementById("edit-desc")).value;
    var payload = JSON.stringify({
      name,
      description: desc,
      id
    })
    var headers = new HttpHeaders({ "content-type": "application/json" })

    this.gs.editGroup(payload, headers)
      .subscribe(res => {
        (<Group>this.group).name = name;
        (<Group>this.group).description = desc;
        this.editingGroup = false;
      }, rej => {
        console.log(rej)
      })
  }

  modUser(member: any) {
    var groupId = (<Group>this.group).id;
    var payload = JSON.stringify({
      userId: member.id,
      groupId,
      status: 1
    })
    var headers = new HttpHeaders({ "content-type": "application/json" });
    this.gs.editModStatus(payload, headers)
      .subscribe(res => {
        console.log(res)
        member.id = 1
      }, rej => {
        console.log(rej)
      })
  }

  kickUser(id: number, self: boolean) {
    var groupId = (<Group>this.group).id
    this.gs.removeUserFromGroup(id, groupId)
      .subscribe(res => {
        console.log(res);
        if (self)
          this.router.navigate(["/home"])
        else {
          this.members = this.members.filter(m => m.id != id)
        }
      }, rej => {
        console.log(rej)
      })
  }

  uploadBackground(event: any) {
    var gId = (<Group>this.group).id
    let asyncDelete: Observable<any> = this.uploadService.deleteImageById(gId);
    asyncDelete.subscribe();
    const file: File = event.target.files[0];
    console.log(file);
    this.fileName = file.name;
    var groupId = (<Group>this.group).id as unknown as string
    let formData = new FormData();
    formData.append("imageFile", file);
    formData.append("userId", this.userId)
    formData.append("groupId", groupId);
    let asyncPostImage: Observable<any> = this.uploadService.uploadImage(formData);
    asyncPostImage.subscribe(res => {
      let asyncDisplayImage: Observable<any> = this.uploadService.getImageByGroupId(gId);
      asyncDisplayImage.subscribe(fileReturned => {
        this.data = fileReturned;
        this.image = this.data.image;
        return this.imageUrl = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,' + this.image);
      })
    });

  }
}


