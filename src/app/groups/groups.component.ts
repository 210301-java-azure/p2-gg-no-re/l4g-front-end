import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { GroupsService } from './groups.service';
import { UsersService } from '../services/users.service';
import { HttpHeaders } from '@angular/common/http';
import { MatGridListModule } from '@angular/material/grid-list';

@Component({
  selector: 'app-group',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css'],
  providers: [
    GroupsService
  ]
})
export class GroupsComponent implements OnInit {
  creatingGroup = false;
  asyncGroups: Observable<Group[]> = this.gs.getAllGroups();
  selectedGroup: Group | null = null;
  groupsList: Group[] = [];
  groupNames: string[] = [];
  username: any = sessionStorage.getItem("username");
  userId: any = sessionStorage.getItem("userId");
  userInfo = {
    username: "",
    email: "",
    id: "",
    totalDiscussions: this.gs.totalDisSubject.value
  };
  editingUserInfo = false;
  editMessage = "";
  tiles: Tile[] = [
    { rows: 2, cols: 3 },
    { rows: 1, cols: 1 }
  ]


  constructor(private router: Router, private gs: GroupsService, private userService: UsersService) {
  }

  ngOnInit(): void {

    const userId = sessionStorage.getItem("userId");

    if (sessionStorage.getItem("userId") !== null) {

      // Populates Groups on Page
      let asyncGroups: Observable<any> = this.gs.getGroupsByUserId(userId);
      asyncGroups.subscribe(groupsReturned => {
        this.groupsList = groupsReturned[0].groups;

        // Store user's group names in this.groupNames

        for (let i = 0; i < groupsReturned.length; i++) {
          for (let j = 0; j < groupsReturned[i].groups.length; j++) {
            this.groupNames.push(groupsReturned[i].groups[j].name);
          }
        }
      }, err => {
        console.log("something went wrong: " + err);
      });

      // Populates User Profile Information
      let asyncUserProfileInfo: Observable<any> = this.userService.getUserById(userId);
      asyncUserProfileInfo.subscribe(userReturned => {
        this.userInfo.email = userReturned.email;
        this.userInfo.username = userReturned.username;
        this.userInfo.id = userReturned.id;
      }, err => {
        console.log("there was a problem getting the user profile information" + err);
      })

      // Get User's Discussion Total
      let asyncDiscussionTotal: Observable<any> = this.userService.getAllDiscussionsByUserId(userId);
      asyncDiscussionTotal.subscribe(discussionsReturned => {
        this.userInfo.totalDiscussions = discussionsReturned.length;
        this.gs.totalDisSubject.next(discussionsReturned.length);
      }, err => {
        console.log("there was a problem getting the user's discussion total: " + err);
      })
    }

    this.gs.totalDisSubject.asObservable().subscribe(val => {
      this.userInfo.totalDiscussions = val;
    })

  }

  createGroup() {
    this.creatingGroup = true;
  }

  cancelCreateGroup() {
    this.creatingGroup = false;
  }

  submitCreateGroup() {
    // var xwwwHeaders = new HttpHeaders({ "content-type": "application/form-data" });
    let formData = new FormData();
    var groupName = <HTMLInputElement>document.getElementById("groupName");
    var description = <HTMLInputElement>document.getElementById("description");
    formData.append("userId", this.userId);
    formData.append("name", groupName.value);
    formData.append("description", description.value);
    this.gs.creatNewGroup(formData).subscribe(res => {
      this.ngOnInit();
    });
  }

  selectGroup(g: Group) {
    this.gs.setGroup(g);
    this.selectedGroup = g;
    this.router.navigate(["/groups/home"])
  }

  unselectGroup() {
    this.gs.setGroup(null);
    this.selectedGroup = null;
    this.router.navigate(['/groups'])
  }

  editUserInfo() {
    this.editingUserInfo = true;
  }

  cancelEditUser() {
    this.editingUserInfo = false;
  }

  submitEditUser() {
    var username = <HTMLInputElement>document.getElementById("username");
    var email = <HTMLTextAreaElement>document.getElementById("email");
    var password = <HTMLTextAreaElement>document.getElementById("password");

    var userRole: any = 2;  // 1: Admin, 2: end-user (default)
    if (sessionStorage.getItem("token") == "admin-token") {
      userRole = 1;
    }

    var payload = JSON.stringify({
      id: this.userInfo.id,
      username: username.value,
      email: email.value,
      password: password.value,
      userRole
    })

    var headers = new HttpHeaders({ "content-type": "application/json" });


    this.userService.editUserInfo(payload, headers, this.userInfo.id).subscribe(res => {
      console.log(res);
      this.editMessage = "success.";
      sessionStorage.setItem("username", username.value)
      this.ngOnInit();
    }, err => {
      this.editMessage = "something went wrong.";
      console.log("something went wrong" + err.message);
    });
  }
}

export interface Group {
  name: string,
  id: number,
  description: string | null,
  numMembers: number,
  userStatus: number
}

export interface Tile {
  rows: number;
  cols: number;
}

