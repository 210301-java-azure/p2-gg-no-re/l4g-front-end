import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Group, GroupsComponent } from '../groups.component';
import { GroupsService } from '../groups.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  group: Group | null = null;
  messages: any[] = [];
  username: string = <string>sessionStorage.getItem("username");
  socket?: WebSocket;
  message: string = "";

  constructor(private gs: GroupsService, private router: Router) { }

  ngOnDestroy(): void {
    this.socket?.close()
  }

  ngOnInit(): void {
    this.group = this.gs.getGroup();
    if (this.group == null) {
      this.router.navigate(["/groups"])
      return;
    }

    this.socket = new WebSocket(`ws://40.76.233.99:7000/messages/${this.group?.name}`)

    this.socket.onopen = (e) => {
      var msg = {
        user: "[System]",
        text: "Connection has opened"
      }
      this.messages.unshift(msg);
    }

    this.socket.onmessage = (e) => {
      if (this.messages.length > 100)
        this.messages.pop();
      this.messages.unshift(JSON.parse(e.data))
    }

    this.socket.onerror = e => {
      var msg = {
        user: "[System]",
        text: "An error has occurred"
      }
      this.messages.unshift(msg);
    }

    this.socket.onclose = (e) => {
      var msg = {
        user: "[System]",
        text: "Connection has been closed"
      }
      this.messages.unshift(msg);
    }
  }

  submitMessage() {
    console.log("submit message" + this.message)
    var msg = {
      user: this.username,
      text: this.message
    }
    this.socket?.send(JSON.stringify(msg));
    this.messages.unshift(msg);
    this.message = "";
  }
}
