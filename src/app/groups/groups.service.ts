import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Group } from './groups.component';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {
  group: Group | null = null;
  groupsUrl: string = environment.baseUrl + "groups";
  groupsIdUrl: string = "localhost:8082/users/3/groups";
  totalDisSubject = new BehaviorSubject(0);

  constructor(private http: HttpClient) { }

  setGroup(g: Group | null) {
    this.group = g;
  }

  getGroup(): Group | null {
    return this.group
  }

  getAllGroups(): Observable<Group[]> {
    console.log("endpoint " + this.groupsUrl)
    return this.http.get<Group[]>(this.groupsUrl);
  }

  creatNewGroup(formData: FormData): Observable<any> {
    return this.http.post(environment.baseUrl + `groups`, formData);
  }

  getGroupsByUserId(userId: any): Observable<any> {
    return this.http.get(environment.baseUrl + `users/${userId}/groups`);
  }

  getGroupDiscussionsById(id: number): Observable<any> {
    return this.http.get(environment.baseUrl + `discussions/${id}/group`);
  }

  getDiscussionCommentsById(id: number): Observable<any> {
    return this.http.get(environment.baseUrl + `comments/discussion/${id}`)
  }



  // getAllUserGroups(): Observable<UserGroup[]> {
  //   return this.http.get<UserGroup[]>(this.userGroupUrl)
  // }

  // checkForUser(userId: number, groupId: number): Observable<UserGroup> {
  //   return this.http.get<UserGroup>(this.userGroupUrl + `user/${userId}/group/${groupId}`)
  // }

  postNewDiscussion(payload: any, headers: HttpHeaders): Observable<any> {
    return this.http.post(environment.baseUrl + `discussions`, payload, {
      headers: headers
    });
  }

  postNewComment(payload: any, headers: HttpHeaders): Observable<any> {
    return this.http.post(environment.baseUrl + `comments`, payload, {
      headers: headers
    })
  }

  joinGroup(payload: any, headers: HttpHeaders): Observable<any> {
    return this.http.post(environment.baseUrl + "user-group", payload, {
      headers: headers
    })
  }

  deleteDiscussion(id: number): Observable<any> {
    return this.http.delete(environment.baseUrl + `discussions/${id}`)
  }

  deleteComment(id: number): Observable<any> {
    return this.http.delete(environment.baseUrl + `comments/${id}`)
  }

  searchName(name: string): Observable<any> {
    return this.http.get(environment.baseUrl + `groups?name=${name}`)
  }

  getUsersOfGroup(id: number): Observable<any> {
    return this.http.get(environment.baseUrl + `users/groups/${id}`)
  }

  removeUserFromGroup(userId: number, groupId: number): Observable<any> {
    return this.http.delete(environment.baseUrl + `user-group/user/${userId}/group/${groupId}`)
  }

  editModStatus(payload: any, headers: HttpHeaders): Observable<any> {
    return this.http.put(environment.baseUrl + "user-group", payload, { headers: headers })
  }

  editGroup(payload: any, headers: HttpHeaders): Observable<any> {
    return this.http.put(environment.baseUrl + "groups", payload, { headers: headers });
  }
}
