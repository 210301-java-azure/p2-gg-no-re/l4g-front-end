

export const DISCUSSIONS = [
	{
	  id: 1,
	  title: "discussion1",
	  user: "user1",
	  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean quis laoreet odio, nec ultricies felis. Nulla in tempus ligula. Donec ornare, purus sed vehicula auctor, ipsum lectus posuere lacus, nec euismod ipsum augue at magna. Nam arcu est, eleifend eget erat sit amet, lacinia ullamcorper nulla. Nunc congue massa ac leo sodales tincidunt. Nullam convallis tincidunt nibh vel vehicula. Vestibulum consequat, odio sit amet dictum rhoncus, ipsum urna semper augue, et congue magna enim eget orci."
	},
	{
	  id: 2,
	  title: "discussion2",
	  user: "user2",
	  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et purus sollicitudin leo molestie ultricies. Phasellus nec tincidunt mauris. Sed ornare commodo odio, eget lobortis nisi sodales sit amet. Quisque ultricies libero dui, sed sodales erat vestibulum pellentesque. Aenean lobortis ultrices augue, at sodales mauris convallis et. In enim ligula, aliquet ut eros nec, ultrices elementum quam. Nulla aliquet mauris nibh, eu bibendum est luctus non. Sed nunc dui, accumsan quis metus quis, faucibus eleifend risus. Curabitur sodales, nulla vitae congue porta, dolor mi malesuada ante, at tempus ante quam sit amet tortor."
	},
	{
	  id: 3,
	  title: "discussion3",
	  user: "user3",
	  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et purus sollicitudin leo molestie ultricies."
	}
  ];

  export const COMMENTS = [
	{
	  id: 1,
	  parent_id: null,
	  user: "user1",
	  text: "pitshpisrt hpsrthpi srthppihnrt",
	  date: new Date(),
	  replies: []
	},
	{
	  id: 2,
	  parent_id: null,
	  user: "user2",
	  text: "rdumytdi,tu,diyd jdtujr htaepih anp",
	  date: new Date(),
	  replies: []
	},
	{
	  id: 3,
	  parent_id: null,
	  user: "user3",
	  text: "pieapthtrlaljbrthb athbo atjohjathrtahrbtlha tehro",
	  date: new Date(),
	  replies: []
	},
	{
	  id: 4,
	  parent_id: 1,
	  user: "user4",
	  text: "aepringiern gaerogbraegbpeipetag jerg",
	  date: new Date(),
	  replies: []
	},
	{
	  id: 5,
	  parent_id: 4,
	  user: "user5",
	  text: "pahnieatin gpiteai etpnitahtr htaepih anp",
	  date: new Date(),
	  replies: []
	},
	{
	  id: 6,
	  parent_id: 2,
	  user: "user6",
	  text: "eanhpi aneptih pae[aot[hte[n hniteanpihtntpnkatkht ejnoaeu tbouear bog oureaogbreoa goearu oure grea ubearou gerouberoabkbhbrhe gikberagb",
	  date: new Date(),
	  replies: []
	},
	{
	  id: 7,
	  parent_id: 5,
	  user: "user7",
	  text: "hrtsh[mrsm hrsmh ryhkys;k;hsyr njoubget greaquoavgerobuetk aetbuoigateougv t e vgtrahrrta rtbthbahuoatvga aeruioeatv",
	  date: new Date(),
	  replies: []
	},
	{
	  id: 8,
	  parent_id: 7,
	  user: "user8",
	  text: "ououbuOBUOBU obuobrrbabooburoeobugeaejblaejl gaekbjgaeruor agrejgbaerjgb ejbra grg aerj gaerajg jaereagetbjogae rabgaers",
	  date: new Date(),
	  replies: []
	},
	{
	  id: 9,
	  parent_id: 4,
	  user: "user9",
	  text: "apireng piaernpgeargeabgrobgabe booaubg reagobrea earo gare gaergaerogrea graegberjgjktrhbatebkhaerg jhdfgeat grae g",
	  date: new Date(),
	  replies: []
	}
  ]