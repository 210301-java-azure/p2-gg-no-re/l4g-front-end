import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Group } from '../groups.component';
import { GroupsService } from '../groups.service';
import { DiscussionComment } from './comment/comment.component';
import { COMMENTS, DISCUSSIONS } from './mock-discussions';

@Component({
  selector: 'app-discussions',
  templateUrl: './discussions.component.html',
  styleUrls: ['./discussions.component.css']
})

export class DiscussionsComponent implements OnInit {
  makingNewDiscussion : boolean = false;
  discussions: any[] = [];
  selectedDiscussion?: any;
  comments? : DiscussionComment[] = [];
  userId : number = parseInt(<string>sessionStorage.getItem("userId"))
  totalDiscussions = 0;

  constructor(private gs : GroupsService, private router : Router) { }

  ngOnInit(): void {
    if(this.gs.getGroup() == null) {
      this.router.navigate(["/groups"])
    }
    else {
      this.gs.getGroupDiscussionsById((<Group>this.gs.getGroup()).id)
      .subscribe(res => {
        console.log(res)
        res.forEach((e : any) => {
          e.description = e.description || "" ;
        })
        this.discussions = res.reverse();
      }, rej => {
        console.log(rej);
      })
    }

    this.gs.totalDisSubject.asObservable().subscribe(val =>{
      this.totalDiscussions = val;
      console.log(this.totalDiscussions);
    })
  }

  selectDiscussion(d: any) {
    console.log("selecting ", d.title);
    this.selectedDiscussion = d;
    this.gs.getDiscussionCommentsById(d.id)
    .subscribe((res : any[]) => {
      res.forEach((e : any) => {
        e.replies = []
      })
      this.parseComments(res)
    }, rej => {
      console.log(rej)
    })
  }

  unselectDiscussion() {
    this.selectedDiscussion = null;
    this.comments = [];
  }

  makeDiscussionComment() {
    var textarea = <HTMLTextAreaElement>document.getElementById("discussion-reply");
    if(textarea.value == "") return;
    var name = sessionStorage.getItem("username");
    var id = sessionStorage.getItem("userId");
    var payload = JSON.stringify({
      discussionId: this.selectedDiscussion.id,
      userId: id,
      parentId: 0,
      username: name,
      message: textarea.value
    })
    var headers = new HttpHeaders({"content-type": "application/json"})
    
    this.gs.postNewComment(payload, headers)
    .subscribe(res => {
      console.log(res);
      res.replies = [];
      textarea.value = "";
      (<DiscussionComment[]>this.comments).unshift(res)
    }, rej => {
      console.log(rej)
    })
  }

  startNewDiscussion() {
    this.makingNewDiscussion = true;
  }

  cancelNewDiscussion() {
    this.makingNewDiscussion = false;
  }

  submitNewDiscussion() {
    var title = <HTMLInputElement>document.getElementById("new-discussion-title");
    var description = <HTMLTextAreaElement>document.getElementById("new-discussion-desc");
    var username = sessionStorage.getItem("username");
    var userId = sessionStorage.getItem("userId");
    var groupId = (<Group>this.gs.getGroup()).id;

    var payload = JSON.stringify({
      title: title.value,
      description: description.value,
      userId: userId,
      username: username,
      groupId: groupId
    })

    var headers = new HttpHeaders()
      .set("content-type", "application/json");


    this.gs.postNewDiscussion(payload, headers)
    .subscribe((res : any) => {
      console.log(res);
      this.discussions.unshift(res);
      this.makingNewDiscussion = false;
      this.gs.totalDisSubject.next(this.totalDiscussions += 1);
    }, (rej : any) => {
      console.log(rej);
    })
  }

  deleteDiscussion() {
    this.gs.deleteDiscussion(this.selectedDiscussion.id)
    .subscribe(res => {
      console.log(res)
      this.selectedDiscussion = undefined;
      this.ngOnInit()
    }, rej => {
      console.log(rej)
    });
  }

  parseComments(comments : DiscussionComment[]) {
    let map: {[key: number] : DiscussionComment} = {};
    let parentArray : DiscussionComment[] = [];
    console.log("parsing comments");
    comments.forEach(c => {
      console.log(c)
      map[c.id] = c
        
      if(c.parentId == 0) {
        parentArray.push(c);
      }
      else if(!map[c.parentId].replies.includes(c)) {
          map[c.parentId].replies.push(c)
      }
    })
    this.comments = parentArray;
  }
}
