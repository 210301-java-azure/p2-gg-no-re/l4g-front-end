import { HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { GroupsService } from '../../groups.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {
  @Input() comment: DiscussionComment | null = null;
  @Input() discussion_id : number = 0;
  editing : boolean = false;
  replying : boolean = false;
  userId : number = parseInt(<string>sessionStorage.getItem("userId"));

  constructor(private gs : GroupsService) { }

  ngOnInit(): void {
  }

  submitReply(parent_id : number) {
    var replyTextArea = (<HTMLTextAreaElement>document.getElementById("reply-text"));
    if(replyTextArea.value == "") return;
    var name = sessionStorage.getItem("username");
    var id = sessionStorage.getItem("userId");
    var date = Date.now();
    var payload = JSON.stringify({
      discussionId: this.discussion_id,
      userId: id,
      parentId: parent_id,
      username: name,
      message: replyTextArea.value,
      datePosted: date
    })
    var headers = new HttpHeaders({"content-type": "application/json"})

    this.gs.postNewComment(payload, headers)
    .subscribe(res => {
      console.log(res);
      res.replies = [];
      (<DiscussionComment>this.comment).replies.unshift(res)
      this.replying = false;
    }, rej => {
      console.log(rej)
    })
  }

  cancelReply() {
    (<HTMLTextAreaElement>document.getElementById("reply-text")).value = "";
    this.replying = false;
  }

  deleteComment() {
    console.log("deleting comment");
    this.gs.deleteComment((<DiscussionComment>this.comment).id)
    .subscribe(res => {
      console.log(res);
      (<DiscussionComment>this.comment).replies = [];
      (<DiscussionComment>this.comment).message = "Deleted"
    }, rej => {
      console.log(rej)
    })
  }
}

export interface DiscussionComment {
  id : number,
  userId: number,
  parentId : number,
  username : string,
  message : string,
  replies : DiscussionComment[]
}