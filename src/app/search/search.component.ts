import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Group } from '../groups/groups.component';
import { GroupsService } from '../groups/groups.service';
import { AuthService } from '../services/auth.service';
import { SearchService } from './search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  isLoggedIn: boolean = false;
  searchQuery: string = "";
  groups: Group[] = [];

  constructor(private ss: SearchService, private gs: GroupsService, private authService: AuthService) { }

  ngOnInit(): void {
    this.gs.getAllGroups()
      .subscribe((res: Group[]) => {
        console.log(res);
        res.forEach(g => {
          g.description = g.description || "";
        })
        this.groups = res;
      }, rej => {
        console.log(rej)
      })

    this.ss.searchQuery.subscribe(val => {
      this.searchQuery = val;
      this.gs.searchName(val)
        .subscribe(res => {
          this.groups = res;
        }, rej => {
          console.log(rej);
          this.groups = [];
        })
    })

    this.authService.isLoggedIn.subscribe(val => {
      this.isLoggedIn = val;
    })
  }

  joinGroup(groupId: number, button: any) {
    var id = sessionStorage.getItem("userId");
    var payload = JSON.stringify({
      userId: parseInt(<string>id),
      groupId,
      status: 2
    })
    this.gs.joinGroup(payload, new HttpHeaders({ "content-type": "application/json" }))
      .subscribe(res => {
        button.innerHTML = "Joined"
      }, rej => {
        button.innerHTML = "Already Joined"
      })
  }
}
