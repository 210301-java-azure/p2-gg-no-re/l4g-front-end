import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  loginUrl: string = environment.baseUrl + "login";
  signupUrl: string = environment.baseUrl + "signup";
  isLoggedIn = new BehaviorSubject(sessionStorage.getItem("token") != null);
  isAdmin = new BehaviorSubject(sessionStorage.getItem("token") == "admin-token");

  constructor(private http: HttpClient) { }

  attemptLogin(email: string, password: string): Observable<any> {
    const payload = new FormData();
    payload.append("email", email);
    payload.append("password", password);


    return this.http.post(this.loginUrl, payload, { observe: "response" });
  }

  attemptSignup(username: string, email: string, password: string): Observable<any> {

    const userRole: any = 2;  // 1: Admin, 2: end-user (default)

    //const payload = `username=${username}&email=${email}&password=${password}&userRole=${userRole}`
    const payload = new FormData();
    payload.append("username", username);
    payload.append("email", email);
    payload.append("password", password);
    payload.append("userRole", userRole);

    return this.http.post(this.signupUrl, payload, { observe: "response" });
  }


  logout() {
    this.isLoggedIn.next(false);
    this.isAdmin.next(false);
    sessionStorage.clear();

  }

}
