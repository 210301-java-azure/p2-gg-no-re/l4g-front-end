import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AdminPortalComponent } from 'src/app//admin-portal/admin-portal.component';
import { User } from '../models/User';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminPortalService {
  admin: number = 1;
  baseUser: number = 2;
  user: User | null = null;
  baseUrl = environment.baseUrl + "users";
  constructor(private http: HttpClient) { }

  deleteUserById(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + `/${id}`, { headers: { "Authorization": "admin-token" } })
  }

  makeAdmin(id: number): Observable<any> {
    const payload = new FormData();
    payload.append("roleId", "1")
    return this.http.put(this.baseUrl + `/role/${id}`, payload, { headers: { "Authorization": "admin-token" } })
  }

  revokeAdmin(id: number): Observable<any> {
    const payload = new FormData();
    payload.append("roleId", "2");
    return this.http.put(this.baseUrl + `/role/${id}`, payload, { headers: { "Authorization": "admin-token" } })
  }

}
