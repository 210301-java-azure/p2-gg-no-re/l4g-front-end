import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SteamNewsItem } from 'src/models/SteamNewsItem';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})

export class SteamService {

  //steamApiCall: string = environment.steamUrl + "steam?appid=440";
  steamCall: string = environment.baseUrl + "/steam?appid=";
  steamAppId: number = 306130;

  constructor(private http: HttpClient) { }

  getSteamNews(): Observable<any> {
    return this.http.get<any>(this.steamCall + this.steamAppId);
  }

  getSteamNewsByAppId(appId: number): Observable<any> {
    return this.http.get<any>(this.steamCall + appId);
  }

  parseNewsHtml(stringHtml: string) {
    const parser = new DOMParser();
    return parser.parseFromString(stringHtml, "text/html");
  }

}
