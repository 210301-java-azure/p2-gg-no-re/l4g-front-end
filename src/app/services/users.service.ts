import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  userUrl = environment.baseUrl + "users";
  discussionUrl = environment.baseUrl + "discussions";

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<any>{
    return this.http.get<User[]>(this.userUrl);
  }

  getUserById(userId: any): Observable<any>{
    return this.http.get(this.userUrl + "/" + `${ userId }`);
  }

  getAllDiscussionsByUserId(userId: any): Observable<any>{
    return this.http.get(this.discussionUrl + "/" + `${ userId }` + "/user");
  }


  //-------------------------------------------------------------------------
  //editUserInfo(payload: any, headers : HttpHeaders, userId: any ): Observable<any>{
  //   const userUrlId = this.userUrl + "/"+ `${userId}`;
  //   return this.http.put(userUrlId, payload, {headers: headers}, {observe : "response"});
  //}
  //-------------------------------------------------------------------------

    editUserInfo(payload : any, headers : HttpHeaders, userId: string){
    return this.http.put(this.userUrl + "/" + `${userId}`, payload, {headers: headers});
  }

}
export interface User {
  id: number,
  username: string,
  userRole: number,
  email: string
}