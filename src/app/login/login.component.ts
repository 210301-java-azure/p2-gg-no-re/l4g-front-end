import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email: string    = "";
  password: string = "";
  message: string  = "";

  //constructor() { }
  constructor(private authService: AuthService, private router: Router) { }


  ngOnInit(): void {
    var email = localStorage.getItem("email");
    if(email != null) {
      this.email = email;
      (<HTMLInputElement>document.getElementById("remember")).checked = true;
    }
  }

  login(){
    this.authService.attemptLogin(this.email, this.password).subscribe(
      (res)=>{
        var check = <HTMLInputElement>document.getElementById("remember");
        if(check.checked) {
          localStorage.setItem("email", this.email);
        }
        else {
          localStorage.clear();
        }
        this.message = "Login successful";
        sessionStorage.setItem("token", res.headers.get("Authorization"));
        sessionStorage.setItem("userId", res.headers.get("User-Id"));
        sessionStorage.setItem("username", res.headers.get("Username"));
        this.authService.isLoggedIn.next(true);

        if(sessionStorage.getItem("token") == "admin-token"){
          this.authService.isAdmin.next(true);
        }

        // navigate to another page
        this.router.navigate(['home']);
      }, 
      (err)=>{
        console.log(err);
        this.message = err.error;
      })
  }

}
