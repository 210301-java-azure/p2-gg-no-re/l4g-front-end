import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UploadService } from 'src/app/upload.service';
import { Observable } from 'rxjs';
import { Byte } from '@angular/compiler/src/util';
import { DomSanitizer } from '@angular/platform-browser';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  fileName = '';
  groupId = 2;
  data: any;
  image: Byte[] = [];
  imageUrl: any;
  userId: any = sessionStorage.getItem("userId");
  uploadProgress: number = 0;



  constructor(private uploadService: UploadService, private domSanitizer: DomSanitizer) { }

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    this.fileName = file.name;
    let formData = new FormData();
    formData.append("imageFile", file);
    formData.append("userId", this.userId)
    formData.append("groupId", "2");
    let asyncPostImage: Observable<any> = this.uploadService.uploadImage(formData);
    asyncPostImage.subscribe();

  }



  ngOnInit(): void {
  }

  getImage() {
    let asyncGetImage: Observable<any> = this.uploadService.getImageByGroupId(this.groupId)
    asyncGetImage.subscribe(fileReturned => {
      this.data = fileReturned;
      this.image = this.data.image;
      return this.imageUrl = this.domSanitizer.bypassSecurityTrustUrl('data:image/jpg;base64,' + this.image);
    });
  }
}
