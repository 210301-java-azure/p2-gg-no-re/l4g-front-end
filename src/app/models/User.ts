export class User {
    
    id: number;
    name: string;
    username: string;
    email: string;
    //password: string

    constructor(_id?:number, _name?:string, _username?:string, _email?:string ){
        this.id       = _id || 0 ;
        this.name     = _name || "";
        this.username = _username || "";
        this.email    = _email || "";
    }
}