import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})


export class SignupComponent implements OnInit {

  username: string = "";
  email: string    = "";
  password: string = "";
  userRole: number = 2;  // 1: Admin, 2: end-user (default)
  message: string  = "";

  //constructor() { }
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  signup(){

    console.log("TEST: username: "+this.username+" Email:"+ this.email+" Password: "+this.password);

    this.authService.attemptSignup(this.username, this.email, this.password).subscribe((res)=>{
      this.message = "Signup successful";
      console.log("successful signup and status "+ res.status); 

      //console.log("get Authorization from the header: "+ res.headers.get("Authorization") );
      // get headers from response
      sessionStorage.setItem("token",res.headers.get("Authorization"));

      // navigate to another page
      this.router.navigate(['login']);
    }, 
    (res)=>{
      console.log(" full error header: "+ JSON.stringify(res)); //temp for test
      console.log("error message: "+ JSON.stringify(res.error.message)); //temp for test
      this.message = res.error.message;
    })
  }

}
