import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { GroupsService } from '../groups/groups.service';
import { SearchService } from '../search/search.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  searchQuery : string = "";
  isLoggedIn = false;
  isAdmin = false;

  constructor(private router: Router, private authService: AuthService, private ss : SearchService, private gs : GroupsService) {}
  
  ngOnInit(): void {
    this.authService.isLoggedIn.asObservable().subscribe(val => {
      this.isLoggedIn = val;
    })
    this.authService.isAdmin.asObservable().subscribe(val => {
      this.isAdmin = val;
    })
  }

  performSearch() {
    this.ss.searchQuery.next(this.searchQuery);
    this.router.navigate(['/search']);
  }

  navToSearch() {
    this.router.navigate(['/search']);
  }

  logout(){
    this.authService.logout();
  }
}
