import { group } from '@angular/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiscussionsComponent } from './groups/discussions/discussions.component';
import { GroupHomeComponent } from './groups/group-home/group-home.component';
import { GroupsComponent } from './groups/groups.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { NewsComponent } from './news/news.component';
import { SearchComponent } from './search/search.component';
import { AdminPortalComponent } from './admin-portal/admin-portal.component';
import { UploadComponent } from 'src/app/upload/upload.component';
import { ChatComponent } from './groups/chat/chat.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: "home", component: HomeComponent, children: [
      { path: "news", component: NewsComponent },
    ]
  },
  { path: "search", component: SearchComponent },
  { path: "login", component: LoginComponent },
  { path: "signup", component: SignupComponent },
  {
    path: "groups", component: GroupsComponent, children: [
      { path: "home", component: GroupHomeComponent },
      { path: "discussions", component: DiscussionsComponent },
      { path: "chat", component: ChatComponent }
    ]
  },
  { path: "admin-portal", component: AdminPortalComponent },
  { path: "upload", component: UploadComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
