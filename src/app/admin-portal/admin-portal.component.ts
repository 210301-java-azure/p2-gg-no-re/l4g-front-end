import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { AdminPortalService } from 'src/app/services/admin-portal.service';

@Component({
  selector: 'app-admin-portal',
  templateUrl: './admin-portal.component.html',
  styleUrls: ['./admin-portal.component.css']
})
export class AdminPortalComponent implements OnInit {
  message: string = "";
  asyncUsers: Observable<any[]> = this.userService.getAllUsers();
  displayedColumns: string[] = ['id', 'username', 'userRole', 'email'];
  usersList: any[] = [];
  isAdmin: boolean = false;
  deletingUser: boolean = false;
  updatingUser: boolean = false;

  constructor(private authService: AuthService, private userService: UsersService, private adminService: AdminPortalService) { }

  ngOnInit(): void {
    this.authService.isAdmin.asObservable().subscribe(val => {
      this.isAdmin = val;
    })

    if (this.isAdmin) {
      this.asyncUsers.subscribe(returnedUsers => {
        for (let user of returnedUsers) {
          this.usersList.push(user);
        }
      }, err => {
        console.log("something went wrong");
      })
    }
  }

  deleteUser() {
    this.deletingUser = true;
  }

  cancelDeleteUser() {
    this.deletingUser = false;
  }

  updateUserRole() {
    this.updatingUser = true;
    console.log(this.updatingUser);
  }

  cancelUpdateUserRole() {
    this.updatingUser = false;
  }

  submitDeleteUser() {
    let userIdToDelete = <HTMLInputElement>document.getElementById("delete-user-with-id");
    let id: number = +userIdToDelete.value;
    this.adminService.deleteUserById(id).subscribe(
      () => { this.message = "Successfully deleted user" },
      () => { this.message = "There was an issue" })
    this.ngOnInit();
  }

  submitGrantAdmin() {
    let userIdToChange = <HTMLInputElement>document.getElementById("update-user-with-id");
    let id: number = +userIdToChange.value;
    this.adminService.makeAdmin(id).subscribe(
      () => { this.message = "Successfully ganted admin" },
      () => { this.message = "There was an issue" })
    this.ngOnInit();
  }

  submitRevokeAdmin() {
    let userIdToChange = <HTMLInputElement>document.getElementById("update-user-with-id");
    let id: number = +userIdToChange.value;
    this.adminService.revokeAdmin(id).subscribe(
      () => { this.message = "Successfully ganted admin" },
      () => { this.message = "There was an issue" })
    this.ngOnInit();
  }
}
