import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SteamService } from 'src/app/services/steam.service';
import { AuthService } from '../services/auth.service';
// import { SteamNewsItem } from 'src/models/SteamNewsItem';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  isLoggedIn : boolean = false;
  // Perhaps we'll map this with ObjectMapper on the server side
  //steamItem: SteamNewsItem | object = {}
  asyncNewsObject: Observable<any> = this.steamService.getSteamNews();
  asyncNewsObject1: Observable<any> = this.steamService.getSteamNewsByAppId(359550);
  asyncNewsObject2: Observable<any> = this.steamService.getSteamNewsByAppId(588650);
  asyncNewsObject3: Observable<any> = this.steamService.getSteamNewsByAppId(921570);
  asyncNewsObject4: Observable<any> = this.steamService.getSteamNewsByAppId(212680);
  newsObject: any | null = null;
  newsObject1: any | null = null;
  newsObject2: any | null = null;
  newsObject3: any | null = null;
  newsObject4: any | null = null;
  errorMessage: string = "";
  steamAppId: number = 0;
  newsHtml: any | null = null;
  newsHtml1: any | null = null;
  newsHtml2: any | null = null;
  newsHtml3: any | null = null;
  newsHtml4: any | null = null;
  newsParElements: any = [];
  newsParElements1: any = [];
  newsParElements2: any = [];
  newsParElements3: any = [];
  newsParElements4: any = [];
  
  
  
  constructor(private steamService: SteamService, private authService : AuthService) { }

  ngOnInit() : void {
    this.authService.isLoggedIn.asObservable().subscribe(val => {
      this.isLoggedIn = val;
    })
    
    this.asyncNewsObject.subscribe(
      (objectReturned) =>{
        this.newsObject = objectReturned;
        this.setNewsHtml(objectReturned.appnews.newsitems[0].contents);
      },
      () => {this.errorMessage = "Could not retreieve the data"});

    this.asyncNewsObject1.subscribe(
      (objectReturned) =>{
        this.newsObject1 = objectReturned;
        this.newsHtml1 = this.steamService.parseNewsHtml(objectReturned.appnews.newsitems[0].contents);
        for(let i = 0; i < this.newsHtml1.getElementsByTagName('p').length; i++){
          this.newsParElements1.push(this.newsHtml1.getElementsByTagName('p')[i].innerHTML);
        }
      },
      () => {this.errorMessage = "Could not retreieve the data"});

    this.asyncNewsObject2.subscribe(
      (objectReturned) =>{
        this.newsObject2 = objectReturned;
        this.newsHtml2 = this.steamService.parseNewsHtml(objectReturned.appnews.newsitems[0].contents);
        for(let i = 0; i < this.newsHtml2.getElementsByTagName('p').length; i++){
          this.newsParElements2.push(this.newsHtml2.getElementsByTagName('p')[i].innerHTML);
        }
      },
      () => {this.errorMessage = "Could not retreieve the data"});

    this.asyncNewsObject3.subscribe(
      (objectReturned) =>{
        this.newsObject3 = objectReturned;
        this.newsHtml3 = this.steamService.parseNewsHtml(objectReturned.appnews.newsitems[0].contents);
        for(let i = 0; i < this.newsHtml3.getElementsByTagName('p').length; i++){
          this.newsParElements3.push(this.newsHtml3.getElementsByTagName('p')[i].innerHTML);
        }
      },
      () => {this.errorMessage = "Could not retreieve the data"});

    this.asyncNewsObject4.subscribe(
      (objectReturned) =>{
        this.newsObject4 = objectReturned;
        this.newsHtml4 = this.steamService.parseNewsHtml(objectReturned.appnews.newsitems[0].contents);
        for(let i = 0; i < this.newsHtml4.getElementsByTagName('p').length; i++){
          this.newsParElements4.push(this.newsHtml3.getElementsByTagName('p')[i].innerHTML);
        }
      },
      () => {this.errorMessage = "Could not retreieve the data"});
  }

  onEnter(){
    this.steamService.getSteamNewsByAppId(this.steamAppId).subscribe(
      (objectReturned) => {
        this.newsObject = objectReturned;
        this.setNewsHtml(objectReturned.appnews.newsitems[0].contents);
      },
      (err) => {
        this.errorMessage = err;
      })
  }

  setNewsHtml(newsObject: any){
    this.newsHtml = this.steamService.parseNewsHtml(newsObject);
    this.setNewsParagraphs(this.newsHtml);
  }

  setNewsParagraphs(newsHtml: any){
    this.newsParElements = [];
    for(let i = 0; i < newsHtml.getElementsByTagName('p').length; i++){
      this.newsParElements.push(newsHtml.getElementsByTagName('p')[i].innerHTML);
    }
  }

  focusSearchbar() {
    document.getElementById("search-bar")?.focus();
  }
}
