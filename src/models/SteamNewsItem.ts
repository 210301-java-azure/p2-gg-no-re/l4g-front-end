// a Class to store our Steam API call

export class SteamNewsItem {
    appnews: string;
    appid: number;
    title: string;
    contents: string;

    constructor(_appnews?: string, _appid?: number, _title?: string, _contents?: string) {
        this.appnews = _appnews || "";
        this.appid = _appid || 0;
        this.title = _title || "";
        this.contents = _contents || "";
    }
}