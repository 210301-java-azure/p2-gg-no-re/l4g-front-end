### L4G

> As an `admin` I should be able to login, create a group, delete a group, delete users, create posts, events, comment, grant priveleges etc.
- Admins are users that should have control over the site as a whole

> As a `moderator` I should be able to login, manage the group I'm moderating, able to kick users, create posts, events, etc.
- moderators should be "leaders" of the group and have enough priveleges to moderate the group.

> As a `user` I should be able to sign-up, login, join a group, comment, etc.  The site should be intuitive and easy to navigate.
- A user should be able to join a group and be able to comment and share ideas relevant to the group.

---
# Functionality

- The application as a whole should be a single page application utilizing the Angular framework.
- The idea behind the application is to be a video game message board where users with common interests can get together and share guides, ideas, memes, etc.
- The general layout should have:
    - a navigation bar near the top with some options:
        - Sign-up/Login/logout
        - searchbar to find groups
        - home button
    - Some kind of a logo/picture indicating the group
    - tabs within groups for:
        - home
        - events
        - news
        - commenting
        - guides
        - memes
- **Steam API** 
- getting game news from Steam API
- getting Users from Steam API

---
# Afterthoughts/Stretch

- As posts get longer, perhaps implementing pagination would be useful
- Or perhaps deleting posts automatically after they reach an arbitrary number





