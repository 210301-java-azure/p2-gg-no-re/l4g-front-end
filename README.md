### L4G

## What is L4G?
L4G is an application where a user can create an account to browse/join/create groups about their favorite games.
Users can create discussions within groups to start a conversation, where other users can comment.


## Technologies Used
- Angular 11.2.9
- Postman 8.1.0 (Windows)
- HTML/CSS/TypeScript
- Node 14.2.0
- VS Code
- (See Backend Project for Backend Technologies)

### Features
## What's on the Home Page?
- Header Image
- Steam News Cards
	- Working Steam Ids for demo:
	- 1237970 // Titanfall 2
			https://store.steampowered.com/app/1237970/Titanfall_2/
	- 1222140 // Detroit Become Human
	- 1113560 // NieR Replicant
		Doesn’t Work: <br>
683900 // https://store.steampowered.com/app/683900/RollerCoaster_Tycoon_Classic/ <br>
If pre-loaded news cards return unreadable format, will break the other cards <br>
			
- Search Bar (Read Only)

## Login
- must enter valid email
- must enter valid password
- form tells you which to correct (if any)

## Once logged in, what can a user do?
- User's now have access to:
    - Dashboard
		- View and Edit Profile Information
		- View Current Joined Groups
		- Create a new Group

- Search Bar (Can now join existing groups)

# Groups Home
- View Group Banner
- View Group Description
- View Group Members

# Discussions
- View discussions for the chosen group
- View comments on discussions
- Create new discussions on the discussion board
- Add comments
- Can delete discussions and comments if they were created by the user

# Chat messaging
- Upon entering the chat page, the system will connect to the chat room associated with the group
- After successful connection, the user will be able to send messages to communicate with other users in the group who are connected to the chat
- Users are notified by the system when another user connects and disconnects


# What can admins do?
- Everything a user can
- Access to ‘‘Admin Portal’ page
- Can delete users or change their user role (user to admin or vice versa)

# Creators of Groups (Mods) Can:
- Edit Group Description
- Edit Group Banner Image
- Grand Mod privilege to other group members
- Kick members from the group

---
---

## Getting Started
- To get started, make sure you have `Git` installed: [See Here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Once `Git` is installed, navigate to where you'd like to save the repository (Eg. C:\Users\Documents\l4g-repository)
- Open `Git-bash` and run the command: `git clone https://gitlab.com/210301-java-azure/p2-gg-no-re/l4g-front-end.git`
- Once you've cloned the repository, navigate to its folder: `cd (../where you saved the repository)`
-  you'll need to add the necessary dependencies, run the following commands:
	- `npm install`
	- `npm install bootstrap`
- Once these have completed, you should now be able to run the app, run the following command:
	- `ng serve` to start the application, you can open your favorite browser to `http://localhost:4200` to see the application in action
	- alternatively, you can run `ng serve -o` to open a new tab automatically.
- You're all set to start exploring L4G! have fun!

# Contributors
Duncan Asplundh, Derek Martinez, Jason Tan, Jude Nimesh




